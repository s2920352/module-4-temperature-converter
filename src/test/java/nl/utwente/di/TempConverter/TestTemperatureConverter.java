package nl.utwente.di.TempConverter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestTemperatureConverter {

    private TemperatureConverter converter;

    @BeforeEach
    public void setup() {
        converter = new TemperatureConverter();
    }

    @Test
    public void convertToFahrenheit() {
        assertEquals(converter.convertToFahrenheit(100), 212);
        assertEquals(converter.convertToFahrenheit(0), 32);
    }
}
