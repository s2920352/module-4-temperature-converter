package nl.utwente.di.TempConverter;

public class TemperatureConverter {
    public double convertToFahrenheit(double celsius) {
        return (9.0 / 5.0) * celsius + 32;
    }
}
