package nl.utwente.di.TempConverter;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.Serial;

public class TempConverterServlet extends HttpServlet {
    @Serial
    private static final long serialVersionUID = 1L;

    private TemperatureConverter converter;

    public void init() {
        converter = new TemperatureConverter();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
        throws NumberFormatException, IOException {

        String celsiusParameter = request.getParameter("celsius");
        double celsius = Double.parseDouble(celsiusParameter);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write("{\"fahrenheit\":" + converter.convertToFahrenheit(celsius) + "}");
    }
}
