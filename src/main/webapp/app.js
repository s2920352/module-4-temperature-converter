const button = document.getElementById("convert");
const input = document.getElementById("celsius");
const output = document.getElementById("result");

button.addEventListener("click", (e) => {
    e.preventDefault();

    const value = input.value;
    if (value < -273) {
        alert("Celsius value should be -273 or higher");
        return;
    }

    fetch("./temp-converter?celsius=" + value).then(r => r.json()).then((result) => {
        output.innerText = result.fahrenheit + "°F";
    });
})